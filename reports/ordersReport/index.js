const AWS = require("aws-sdk");
const s3 = new AWS.S3();
const ses = new AWS.SES({
  region: "us-west-1"
});

const mysql = require("mysql");
const pool = mysql.createPool({
  host: process.env.host,
  user: process.env.user,
  password: process.env.password,
  database: process.env.database,
  port: process.env.port,
});

    
const fs = require("fs");
const csvjson = require("csvjson");

exports.handler = (event, context, callback) => {
  // Get report request data.
  console.log("event", JSON.stringify(event) );
  //let bucketName = event.detail.requestParameters.bucketName;
  let bucketKey = event.detail.requestParameters.key;
  let reqObjData = event.report.reqObjData;
      console.log(reqObjData);
  if(reqObjData)
  {
    let reportFileName = bucketKey.split(".")[0]+".csv";
    console.log(reportFileName);
    let customerId = reqObjData.customerId;
    let orderStatus = reqObjData.orderStatus;
    let orderType = reqObjData.orderType;
    let from = reqObjData.from;
    let to = reqObjData.to;
    
    console.log('customerId:', customerId);
    // allows for using callbacks as finish/error-handlers
    context.callbackWaitsForEmptyEventLoop = false;
    try {
      
      pool.getConnection(function(err, connection) {
        if (err) {
          console.log("Poll Connection Error:", err);
          throw err; // not connected!
        }
        let sql = "";
        if(orderStatus && orderType)
        {
          sql = "SELECT orders.orderNumber, orders.orderType, orders.shipMethod, orders.orderDate, orders.orderStatus, orders_shipping_addresses.firstName, orders_shipping_addresses.lastName, orders_shipping_addresses.email, orders_shipping_addresses.customerOfficeName FROM orders LEFT JOIN orders_shipping_addresses ON orders.id=orders_shipping_addresses.orderId WHERE orders.customerId ="+customerId+" AND orders.orderStatus = "+orderStatus+" AND orders.orderType = "+orderType+" AND (orders.created_at BETWEEN '"+from+"' AND '"+to+"')";
        }
        else if(orderStatus)
        {
          sql = "SELECT orders.orderNumber, orders.orderType, orders.shipMethod, orders.orderDate, orders.orderStatus, orders_shipping_addresses.firstName, orders_shipping_addresses.lastName, orders_shipping_addresses.email, orders_shipping_addresses.customerOfficeName FROM orders LEFT JOIN orders_shipping_addresses ON orders.id=orders_shipping_addresses.orderId WHERE orders.customerId ="+customerId+" AND orders.orderStatus = "+orderStatus+" AND (orders.created_at BETWEEN '"+from+"' AND '"+to+"')";
        }
        else if(orderType) 
        {
          sql = "SELECT orders.orderNumber, orders.orderType, orders.shipMethod, orders.orderDate, orders.orderStatus, orders_shipping_addresses.firstName, orders_shipping_addresses.lastName, orders_shipping_addresses.email, orders_shipping_addresses.customerOfficeName FROM orders LEFT JOIN orders_shipping_addresses ON orders.id=orders_shipping_addresses.orderId WHERE orders.customerId ="+customerId+" AND orders.orderStatus = "+orderType+" AND (orders.created_at BETWEEN '"+from+"' AND '"+to+"')";
        }
        else
        {
          sql = "SELECT orders.orderNumber, orders.orderType, orders.shipMethod, orders.orderDate, orders.orderStatus, orders_shipping_addresses.firstName, orders_shipping_addresses.lastName, orders_shipping_addresses.email, orders_shipping_addresses.customerOfficeName FROM orders LEFT JOIN orders_shipping_addresses ON orders.id=orders_shipping_addresses.orderId WHERE orders.customerId ="+customerId+" AND (orders.created_at BETWEEN '"+from+"' AND '"+to+"')";
        }
        // Use the connection
        let query = connection.query(sql, function (err, results, fields) {
          // When done with the connection, release it.
          connection.release();
          // Handle error after the release.
          if(err) {
            console.log("Query Connection Error:", err);
            throw err; // query insert error!
          }
          console.log(query.sql);
          console.log('rows:', results);
          let queueData = JSON.stringify(results);
          console.log('queueData:', queueData);

          const csvData = csvjson.toCSV(queueData, {
            delimiter: ",",
            wrap: false,
            headers: 'key'
          });
          console.log(csvData);
          fs.writeFile("/tmp/"+reportFileName, csvData, (err) => {
            if(err) {
              console.log(err); // Do something to handle the error or just throw it
              throw new Error(err);
            }
            console.log("The file has been saved!");
            //uploadFile(reportFileName);

            fs.readFile("/tmp/"+reportFileName, (err, csvFileContent) => {
              if(err) {
                console.log(err);
                throw new Error(err);
              }
              //console.log(csvFileContent);
              let fileObjUrl = "https://voxship-report-files.s3-us-west-1.amazonaws.com/orderReportFiles/"+reportFileName;
              const params = {
                Bucket: "voxship-report-files", // pass your bucket name
                Key: "orderReportFiles/"+reportFileName, // file will be saved as reportFiles folder
                Body: csvFileContent,
                ContentType: 'application/octet-stream',
                ACL: 'public-read'
              };
              console.log('s3 params:', params);
              try {
                s3.upload(params, function(s3Err, fileData) {
                  if(s3Err) {
                    console.log(s3Err);
                    throw new Error(s3Err);
                  }
                  console.log('s3.upload:', fileData);
                  /*try {
                    // send email notification
                    let eParams = {
                      Destination: {
                        ToAddresses: ["sumanmahrjn@gmail.com"]
                      },
                      Message: {
                        Body: {
                          Text: {
                            Data: "Hey! What's up? You got orders CSV report. CLick "+fileObjUrl+" to download it."
                          }
                        },
                        Subject: {
                          Data: "AWS Order Report!!!"
                        }
                      },
                      Source: "suman@colorstoweb.com"
                    };
                    console.log("eParams:", eParams);
                    console.log("===SENDING EMAIL===");
                    let sendEmail = ses.sendEmail(eParams, function(err, data){
                      if(err) console.log("EMAIL error:", err);
                      else {
                        console.log("===EMAIL SENT===");
                        console.log(data);

                        console.log("===EMAIL CODE END===");
                        console.log("send email: ", sendEmail);
                        //context.succeed(event);

                        console.log(`File uploaded successfully at ${fileData.Location}`);
                        callback(null, "Order report file uploaded successfully.");
                      }
                    });
                  } catch(err) {
                    console.log('ses exception error:', err);
                    callback("error", "Order report file saved but issue sending email.");
                  }*/
                  
                  console.log(`File uploaded successfully at ${fileData.Location}`);
                  callback(null, "Order report file uploaded successfully.");

                });
              } catch(err) {
                console.log('s3 exception error:', err);
                callback("error", "Order report file could not be saved in bucket.");
              }
              

            });
          });
        });
      });
      
    } catch (err) {
      console.log(err);
      throw new Error(err);
    }
    //context.succeed(reportFileName);
  }
  else
  {
    console.log("error:", "Empty S3 request data.");
    callback("error", "Empty S3 request data.");
  }
};
