const AWS = require('aws-sdk');
const s3 = new AWS.S3();
exports.handler = (event, context, callback) => {
    console.log('event', JSON.stringify(event) );
    //console.log('key', event.detail.requestParameters.key);
    //callback(null, "Hello, " + event.detail.requestParameters.bucketName + "!");
    // Get existing data.
    let bucketName = event.detail.requestParameters.bucketName;
    let key = event.detail.requestParameters.key;
    s3.getObject({
        Bucket: bucketName,
        Key: key
    }, function(err, data) {
        // Handle any error and exit
        if (err)
        {
            console.log("error:", err);
        }
        else
        {
            // No error happened
            // Convert Body from a Buffer to a String
        
            //let objectData = data.Body.toString('utf-8'); // Use the encoding necessary
            // Parse JSON object.
            //let existingData = JSON.parse(existing.Body);
            //callback(null, "Body, " + objectData.customerId);
            let reqObjData = JSON.parse(data.Body);
            let type = key.split("_")[0];
            callback(null, {
                type,
                reqObjData
            });
        }
    });
};