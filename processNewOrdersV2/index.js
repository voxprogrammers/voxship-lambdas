'use strict';
let mysql = require('mysql');
var axios = require('axios');

let pool = mysql.createPool({
        host: process.env.host,
        user: process.env.user,
        password: process.env.password,
        database : process.env.database,
        port     : 3306,
        queueLimit : 0, // unlimited queueing
        connectionLimit : 0 // unlimited connections 
    });

exports.handler = async (event, context, callback) => {
    //prevent timeout from waiting event loop
    context.callbackWaitsForEmptyEventLoop = false;
    let orderId = event.Records[0].body 
    // let orderId = 24
    let results = [];
    
    // elastic search
    var sql = "SELECT orders.id, orders.customerId, orders.orderNumber, orders.customerName, orders.orderDate, orders.orderStatus, orders.orderType, orders.preSale, orders.isExpedited, orders.updated_at, orders.createdBy, orders_shipping_addresses.firstName, orders_shipping_addresses.middleName, orders_shipping_addresses.lastName, orders_shipping_addresses.email, orders_shipping_addresses.phone1, orders_shipping_addresses.address1 FROM orders INNER JOIN orders_shipping_addresses ON orders.id=orders_shipping_addresses.orderId WHERE orders.id = "+ orderId;
    var order = await performDBQuery(sql,0);
    order = order[0];
     if(order)
     {
         // add addon item if is add on yes and is not exception item
        var sql = "SELECT itemSkuNumber, name FROM catalog_items WHERE customerId = '"+order.customerId+"' AND isAddOnItem = '1' AND isExceptionItem = '0' AND status = '1'";
        var addOnItems = await performDBQuery(sql,0);
        
        if (addOnItems.length > 0) {
            var comment = ''
            for (var i in addOnItems) {
              var val = addOnItems[i];
              comment += val.itemSkuNumber+", ";
                var sql = "INSERT INTO purchased_products (orderId, itemSkuNumber, itemName, itemQty, remainingQtyToBeBoxed, createdBy, updatedBy, created_at, updated_at) VALUES ('"+orderId+"', '"+val.itemSkuNumber+"', '"+val.name+"', '1','1','"+order.createdBy+"','"+order.createdBy+"', NOW(), NOW())";
                await performDBQuery(sql,0);
            }
            if(comment)
            {
                comment += "item(s) Added";
            }
            var sql = "INSERT INTO orders_logs (orderId, orderNumber, orderStatus, customerId, comment, createdBy, updatedBy, created_at, updated_at) VALUES ('"+orderId+"', '"+order.orderNumber+"', '30', '"+order.customerId+"', '"+comment+"', '"+order.createdBy+"','"+order.createdBy+"', NOW(), NOW())";
            await performDBQuery(sql,0);
        }
         
         // add to elastic search
         const data = {
                "type"        : "order",
                "id"          : order.id ? order.id.toString() : '',
                "orderNumber" : order.orderNumber ? order.orderNumber.toString() : '',
                "customerName" : order.customerName ? order.customerName.toString() : '',
                "orderDate"   : order.orderDate ? order.orderDate.toString() : '',
                "orderStatus" : 'On Hold',
                "orderType"   : order.orderType == '0' ? 'Field Office Delivery' : order.orderType == '2' ? 'Vip Delivery' : 'Ship To',
                "preSale"     : order.preSale ? order.preSale.toString() : '',
                "isExpedited" : order.isExpedited ? order.isExpedited.toString() : '',
                "updated_at"  : order.updated_at ? order.updated_at.toString() : '',
                "firstName"   : order.firstName ? order.firstName.toString() : '',
                "middleName"  : order.middleName ? order.middleName.toString() : '',
                "lastName"    : order.lastName ? order.lastName.toString() : '',
                "email"       : order.email ? order.email.toString() : '',
                "phone1"      : order.phone1 ? order.phone1.toString() : '',
                "address1"    : order.address1 ? order.address1.toString() : ''
         }

         const res = await axios.put("https://search-search-6ija5wsv3nxy6s7u75e27vijiq.us-west-1.es.amazonaws.com/search/_doc/"+data.type+data.id, data)
        //  console.log(res)
     }

     // check shipping code if not found mark order as problem order
    var sql = "SELECT shipping_methods.shippingCode, shipping_methods.shippingCompanyId, orders.customerId, orders.orderNumber FROM shipping_methods RIGHT JOIN orders ON orders.shipMethod=shipping_methods.shippingCode WHERE orders.id = "+ orderId;
    var order = await performDBQuery(sql,0);
    order = order[0];
    if(order.shippingCode)
    {
        // update shipping company name in order table
        var sql = "SELECT companyLabel FROM shipping_companies WHERE id = "+ order.shippingCompanyId;
        let shippingCompanies = await performDBQuery(sql,0); 
        let shippingCompany = shippingCompanies[0];
        if(shippingCompany)
        {
            var sqlUpdate = "UPDATE orders SET shippingCompany = '"+shippingCompany.companyLabel+"' WHERE id = "+ orderId;
            await performDBQuery(sqlUpdate,0);
        }
    }
    else
    {
        var sqlUpdate = "UPDATE orders SET problemOrder = '1' WHERE id = "+ orderId +" AND problemOrder = '1'";
        await performDBQuery(sqlUpdate,0);
        var sql = "INSERT INTO problem_order_queues (customerId, orderNumber, errors, type, created_at, updated_at) VALUES ('"+order.customerId+"', '"+order.orderNumber+"', 'Shipping Code Not Matched', '2', NOW(), NOW())";
        await performDBQuery(sql,0);
    }

    var sql = "SELECT id, COUNT(DISTINCT CASE WHEN isPreSale = '1' THEN isPreSale END ) AS isPreSale, COUNT(DISTINCT CASE WHEN isProblemSkuItem = '1' THEN isProblemSkuItem END ) AS isProblemSkuItem FROM purchased_products WHERE orderId = "+ orderId +" GROUP BY orderId";
    results = await performDBQuery(sql,0);

    var result = results[0];
    if(result && (result.isPreSale || result.isProblemSkuItem))
    {
        // if is problem, mark problem order and save in problem_order_queues
        var sql = "UPDATE orders SET preSale = '"+result.isPreSale+"', problemOrder = '"+result.isProblemSkuItem+"' WHERE id = "+ orderId;
        await performDBQuery(sql,1);
        
        if(result.isProblemSkuItem)
        {
            var sql = "SELECT GROUP_CONCAT(itemSkuNumber SEPARATOR ', ') AS errors FROM `purchased_products` WHERE orderId = '"+orderId+"' AND isProblemSkuItem = '1' group by orderId";
            let error1 = await performDBQuery(sql,0); 
            let errors = error1[0].errors 
            
            var sql = "INSERT INTO problem_order_queues (customerId, orderNumber, errors, type, created_at, updated_at) VALUES ((select customerId from orders WHERE id = '"+orderId+"'), (select orderNumber from orders WHERE id = '"+orderId+"'), '"+errors+"', '2', NOW(), NOW())";
            await performDBQuery(sql,0); 
        }
    } else {
        let updateResponse = {};
        var sql = "UPDATE orders SET preSale = '"+result.isPreSale+"', problemOrder = '"+result.isProblemSkuItem+"', isReadyToBatch = '1' WHERE id = "+ orderId;
        updateResponse = await performDBQuery(sql,2);

        if(updateResponse.affectedRows > 0) {
            var sql = "SELECT itemSkuNumber, itemQty FROM purchased_products WHERE orderId = '"+orderId+"' ORDER BY purchased_products.itemSkuNumber ASC";
            var purchasedProductsItems = await performDBQuery(sql,0);
            
            if (purchasedProductsItems.length > 0) {
                var lineItem = ''
                for (var i in purchasedProductsItems) {
                  var val = purchasedProductsItems[i];
                  lineItem += val.itemSkuNumber+"|"+val.itemQty+"|";
                }
                
                var sql = "SELECT orders.customerId, orders.shipMethod, orders_shipping_addresses.country FROM orders INNER JOIN orders_shipping_addresses ON orders.id = orders_shipping_addresses.orderId WHERE orders.id = "+ orderId;
                let orderDetails = await performDBQuery(sql,0);
                var order = orderDetails[0];
                if(order && order.country == 'US')
                {
                    var toHashedValue = lineItem+"|"+order.customerId+"|"+order.shipMethod+"|"+order.country;
                    var sqlUS = "UPDATE orders SET orderHash = MD5('"+toHashedValue+"') WHERE id = "+ orderId;
                    await performDBQuery(sqlUS,0);
                }
                else
                {
                    var toHashedValue = lineItem+"|"+order.customerId+"|"+order.shipMethod;
                    var sqlUS = "UPDATE orders SET orderHash = MD5('"+toHashedValue+"') WHERE id = "+ orderId;
                    await performDBQuery(sqlUS,0);
                }
            }
            
            // var sql = "SELECT CASE WHEN orders_shipping_addresses.country = 'US' THEN MD5(GROUP_CONCAT(CONCAT_WS( '|', orders.customerId, orders.shipMethod, purchased_products.itemSkuNumber, purchased_products.itemQty, orders_shipping_addresses.country ))) ELSE MD5(GROUP_CONCAT(CONCAT_WS( '|', orders.customerId, orders.shipMethod, purchased_products.itemSkuNumber, purchased_products.itemQty ))) END as hashedOrder FROM orders JOIN purchased_products ON orders.id = purchased_products.orderId JOIN orders_shipping_addresses ON orders.id = orders_shipping_addresses.orderId WHERE orders.id = "+ orderId +" GROUP BY purchased_products.orderId ORDER BY purchased_products.itemSkuNumber ASC";
            // let hashed = await performDBQuery(sql,0);
            // var sql = "UPDATE orders SET orderHash = '"+hashed[0].hashedOrder+"' WHERE id = "+ orderId;
            // await performDBQuery(sql,0);
        }
    }
    
};

let performDBQuery = async (sql, params) => {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            if (err) { reject(err); }
            connection.query(sql, params, (err, results) => {
                if (err) { reject(err); }
                connection.release();
                resolve(results);
            });
        });
    });
};