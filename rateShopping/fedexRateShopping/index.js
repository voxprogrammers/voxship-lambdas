// fedex shipment
//let req = require("request");
let path = require('path');
let soap = require('soap');

let url = path.join(__dirname, 'wsdl', 'RateService_v20.wsdl');

exports.handler = (event, context, callback) => {
  //console.log('event', JSON.stringify(event) );
  let date = new Date();
  let messageBody = event;
  //let messageBody = JSON.parse(event.body);
  //console.log("messageBody", messageBody);
  if (messageBody.shippingCompanies.indexOf("FEDEX") > -1) {
    let FEDEX = [];

    let params = {
        'WebAuthenticationDetail': {
            'UserCredential': {
                'Key': 'NopvR84QipkcsK34', //Your Key given by FedEx
                'Password': 'vJizkgKAquhoIanv5WdErMkZh' //Your Password given by FedEx
            }
        },
        'ClientDetail': {
            'AccountNumber': '259407907', //Your Account Number given by FedEx
            'MeterNumber': '108181162' //Your Meter Number given by FedEx
        },
        'Version': {
            'ServiceId': 'crs',
            'Major': '20', 
            'Intermediate': '0',
            'Minor': '0'
        },
        'ReturnTransitAndCommit': true,
        'RequestedShipment': {
            'ShipTimestamp': new Date(date.getTime() + (24*60*60*1000)).toISOString(),
            'DropoffType': 'REGULAR_PICKUP',
            'TotalWeight': {
                'Units': 'LB',
                'Value': parseFloat(messageBody.weight)
            },
            'Shipper': {
                'Address': {
                    'StreetLines': [
                    'Address Line 1'
                    ],
                    'City': messageBody.companyFromAddress.companyCity,
                    'StateOrProvinceCode': messageBody.companyFromAddress.companyState,
                    'PostalCode': messageBody.companyFromAddress.companyZip,
                    'CountryCode': messageBody.companyFromAddress.companyCountry
                }
            },
            'Recipient': {
                'Address': {
                    'StreetLines': messageBody.shippingAddress.address1+" "+messageBody.shippingAddress.address2 || "",
                    'City': messageBody.shippingAddress.city,
                    'StateOrProvinceCode': messageBody.shippingAddress.state,
                    'PostalCode': messageBody.shippingAddress.zip,
                    'CountryCode': messageBody.shippingAddress.country
                }
            },
            'ShippingChargesPayment': {
                'PaymentType': 'SENDER',
                'Payor': {
                    'ResponsibleParty': {
                        'AccountNumber': '259407907' //Your Account Number given by FedEx
                    }
                }
            },
            'RateRequestTypes': 'LIST',
            'PackageCount': '1',
            'RequestedPackageLineItems': {
                'GroupPackageCount': 1,
                'Weight': {
                    'Units': 'LB',
                    'Value': parseFloat(messageBody.weight)
                },
                'Dimensions': {
                    'Length': parseFloat(messageBody.length),
                    'Width': parseFloat(messageBody.width),
                    'Height': parseFloat(messageBody.height),
                    'Units': "IN"
                }
            }
        }
    };

    console.log('params',JSON.stringify(params));
    try {
        soap.createClient(url, function(err, client) {
            client.getRates(params, function(err, result) {
                //console.log(result)
                //res.json(result);
                if(err) {
                    console.log(err);
                    callback(null, err);
                } else {
                    if((result.HighestSeverity != "FAILURE") && (result.HighestSeverity != "ERROR") && (result.HighestSeverity != "WARNING")) {
                        if(Array.isArray(result.RateReplyDetails)) {
                            result.RateReplyDetails.forEach(rate => {
                                let serviceType = rate.ServiceType;
                                let shippingMethod = serviceType.replace("_", " ");
                                
                                FEDEX.push({
                                    shippingMethod: shippingMethod,
                                    serviceType: serviceType,
                                    packageType: rate.PackagingType,
                                    cost: rate.RatedShipmentDetails[0].ShipmentRateDetail.TotalNetCharge.Amount,
                                    deliverDays: rate.DeliveryDayOfWeek?rate.DeliveryDayOfWeek:"",
                                    deliveryDate: rate.DeliveryTimestamp?rate.DeliveryTimestamp:""
                                });
                            });
                            //console.log("Rate Shopping", FEDEX);
                            callback(null, {FEDEX});
                        } else {
                          //console.log("Rate response not array");
                          callback(null, "Rate response not array");
                        }
                    } else {
                        callback(null, result.Notifications);
                    }
                }
            });
        });
    } catch(err) {
        //console.log(err);
        callback(null, err);
    }

  } else {
    callback(null, "Fedex not found in shipping companies");
  }
  
};

